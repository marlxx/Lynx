#include "cmd.h"
#include "logger.h"
#include "convar.h"

namespace Lynx {

	std::vector<Cmd::Command> Cmd::cmdReg = std::vector<Cmd::Command>();
	int Cmd::argc = 0;
	char *Cmd::argv[CMD_MAX_ARGS];

	void Cmd::Init()
	{
		RegisterCommand("echo", Echo);
	}

	void Cmd::RegisterCommand(const char *name, cmdfunc_t func)
	{
		Command c;
		c.name = name;
		c.func = func;
		cmdReg.push_back(c);
	}

	void Cmd::Exec(const char *cmd)
	{
		std::string cmdname;
		tokenizeStr(cmd);

		cmdname = argv[0];

		if(Convar::FindVar(cmd) != NULL)
			return; // Not supported yet

		for(int n = 0; n < cmdReg.size(); n++) {
			if(cmdReg[n].name == cmdname) {
				cmdReg[n].func();
				return;
			}
		}

		log_info("Command not found : %s", cmdname.c_str());
	}

	void Cmd::Echo()
	{
		if(argc < 2) {
			log_info("Usage : echo <string>");
		}

		for(int i = 1; i = argc; i++) {
			log_info("%s", argv[i]);
		}
	}

	void Cmd::tokenizeStr(const char *str)
	{
		const char *c;
		char *buf = tokenbuf;

		argc = 0;

		c = str;
		while ( 1 ) {
			while( 1 ) {
				if(c[0] == ' ')
					c++;

				if(c[0] == '/' && c[1] == '/')
					return;
				else if(c[0] == '/' && c[1] == '*')
					c++;
				else
					break;
			}

			if(c[0] == '"') {
				argv[argc] = buf;
				argc++;

				while(*c && c[0] != '"') {
					*buf++ = *c++;
				}
				*buf++ = 0;

				if(!*c)
					return;
			}

			argv[argc] = buf;
			argc++;

			while( *c != ' ') {
				*buf++ = *c++;
			}

			*buf++ = 0;

			if(!*c)
				return;
		}
	}
}
