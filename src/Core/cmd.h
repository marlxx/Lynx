#ifndef LYNX_CMD_H
#define LYNX_CMD_H

#include <string>
#include <vector>

#define CMD_MAX_ARGS 4
#define CMD_TOKENBUFSIZE 65536

namespace Lynx {

	class Cmd {
	public:
		typedef void (*cmdfunc_t)(void);
		struct Command {
			std::string name;
			cmdfunc_t func;
		};

		static void Init();

		static void RegisterCommand(const char *name, cmdfunc_t func);
		static Command *GetCommand(const char *name);

		static void Exec(const char *cmd);

		static void Echo();
	private:
		static void tokenizeStr(const char *str);
	private:
		static std::vector<Command> cmdReg;
		static int argc;
		static char *argv[CMD_MAX_ARGS];
		static char tokenbuf[65536];
	};
}

#endif // LYNX_CMD_H
