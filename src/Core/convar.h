#ifndef LYNX_CVAR_H
#define LYNX_CVAR_H

#include <unordered_map>
#include <string>
#include "Events/event.h"

namespace Lynx {

	class Convar {
	public:
		struct Var {
			std::string name;
			std::string string;
			float value;
		};

		static void RegisterVar(Var *var);
		static void SetString  (std::string name, std::string string);
		static void SetValue   (std::string name,  float value);

		static std::string GetString(std::string name);
		static float GetValue (std::string name);
		static Var  *FindVar  (std::string name);

	private:
		static std::unordered_map<std::string, Var*> cvars;
	};

	// Dispatch an event every time a cvar is changed for optimization
	class CvarChangedEvent : public Event {
	public:
		CvarChangedEvent(Convar::Var *var) : Event(CvarChanged), m_var(var) {}

		Convar::Var *m_var;
	};
}

#endif // LYNX_CVAR_H
