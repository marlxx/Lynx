#include <stdio.h>
#include <map>
#include "convar.h"
#include "event_manager.h"

namespace Lynx {

	std::unordered_map<std::string, Convar::Var*> Convar::cvars;

	Convar::Var *Convar::FindVar(std::string name)
	{
		if(cvars.find(name) == cvars.end())
			return NULL;

		return cvars[name];
	}

	void Convar::RegisterVar(Convar::Var *variable)
	{
		if(FindVar(variable->name) != NULL)
			return;

		cvars.emplace(variable->name, variable);
	}

	void Convar::SetString(std::string name, std::string string)
	{
		Var *var = FindVar(name);
		if(var == NULL)
			return;

		cvars[name]->string = string;
		EventManager::SendEvent(CvarChangedEvent(var));
	}

	void Convar::SetValue(std::string name, float value)
	{
		Var *var = FindVar(name);
		if(var == NULL)
			return;

		var->value = value;
		EventManager::SendEvent(CvarChangedEvent(var));
	}

	float Convar::GetValue(std::string name)
	{
		Var *var = FindVar(name);
		if(var == NULL)
			return 0;

		return var->value;
	}

	std::string Convar::GetString(std::string name)
	{
		Var *var = FindVar(name);
		if(var == NULL)
			return NULL;

		return var->string;
	}
}
